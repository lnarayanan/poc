package com.lnt.simplify.jsonparser.giturlcreator;

import org.apache.wink.json4j.JSONArray;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class CreateGitUrlFromGitlabJson {

    public static void main(String[] args) throws Exception {
        String inputJsonPath = "/Users/admin/Workspace/intellijWorkspace/poc/lnt-simplify-work/inputs/gitlab.json";
        FileReader fr = new FileReader(inputJsonPath);
        JSONArray obj = new JSONArray(fr);
        for (Object project : obj) {
            JSONObject proj = (JSONObject) project;
            String relativePath = proj.getString("relative_path");
            System.out.println("git clone https://gitlab.com" + relativePath + ".git");
        }
        fr.close();
    }
}
